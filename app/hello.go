package app

import (
	"fmt"
	"time"
)

type CustomerRQ struct {
	Customer Customer `json:"request_body"`
}

type Customer struct {
	Name     *string    `json:"name,omitempty"`
	Gender   *string    `json:"gender,omitempty"`
	BirthDay *time.Time `json:"birth_day,omitempty"`
	Age      *int       `json:"age,omitempty"`
	Status   *string    `json:"status,omitempty"`
}

type CustomerRes struct {
	Customer Customer `json:"response_body"`
}

func Hello(cust Customer) CustomerRes {

	if cust.Status != nil {
		fmt.Println("is Have Status.")
	}

	if cust.Name != nil {
		fmt.Println("Name is Not null")
	} else {
		fmt.Println("Name is Null")
	}

	return CustomerRes{
		Customer: cust,
	}
}
